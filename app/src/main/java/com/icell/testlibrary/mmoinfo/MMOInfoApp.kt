package com.icell.testlibrary.mmoinfo

import android.app.Application
import com.icell.testlibrary.mmoinfo.di.Injector
import timber.log.Timber

class MMOInfoApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Injector.init(this)
        Timber.plant(Timber.DebugTree())
    }
}