package com.icell.testlibrary.mmoinfo

interface Constants {

    companion object {
        const val BASE_URL = "https://www.mmobomb.com/"
        const val GAME_DETAILS_BUNDLE = "GAME_DETAILS_BUNDLE"
    }
}