package com.icell.testlibrary.mmoinfo.extensions

import com.icell.testlibrary.mmoinfo.interfaces.DisposableStore
import io.reactivex.disposables.Disposable

fun Disposable.store(disposableStore: DisposableStore) = disposableStore.store(this)