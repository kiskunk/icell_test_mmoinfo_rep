package com.icell.testlibrary.mmoinfo.ui.gamedetails

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.icell.testlibrary.mmoinfo.R
import com.icell.testlibrary.mmoinfo.model.GameScreenShot

class GameImagesAdapter(
    private var context: Context,
    private var items: MutableList<GameScreenShot> = mutableListOf(),
    private val listener: GameScreenShotListener?
) : RecyclerView.Adapter<GameImagesAdapter.ViewHolder>() {

    interface GameScreenShotListener {
        fun onScreenShotClick(selectedImage: Int, allImages: List<String?>)
    }

    fun load(screenshots: List<GameScreenShot>) {
        items.clear()

        if (screenshots.isEmpty()) {
            notifyDataSetChanged()
            return
        }

        items.addAll(screenshots)

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.game_screenshot_layout, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val gameScreenShot = items[position]

        Glide.with(context).load(gameScreenShot.image).centerCrop().placeholder(R.drawable.placeholder).into(viewHolder.ivScreenShot)

        viewHolder.root.setOnClickListener {
            if (!gameScreenShot.image.isNullOrEmpty()) {
                listener?.onScreenShotClick(position, items.map { it.image })
            }
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val root: CardView = view.findViewById(R.id.cvRoot)
        val ivScreenShot: ImageView = view.findViewById(R.id.ivScreenShot)
    }
}