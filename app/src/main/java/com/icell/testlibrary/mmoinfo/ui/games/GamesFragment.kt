package com.icell.testlibrary.mmoinfo.ui.games

import android.os.Bundle
import android.view.View
import android.viewbinding.library.fragment.viewBinding
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.icell.testlibrary.mmoinfo.Constants
import com.icell.testlibrary.mmoinfo.R
import com.icell.testlibrary.mmoinfo.databinding.FragmentGamesBinding
import com.icell.testlibrary.mmoinfo.di.Injector
import com.icell.testlibrary.mmoinfo.extensions.hide
import com.icell.testlibrary.mmoinfo.extensions.show
import com.icell.testlibrary.mmoinfo.extensions.smoothAndFastScrollToPosition
import com.icell.testlibrary.mmoinfo.ui.base.BaseFragment
import javax.inject.Inject

class GamesFragment : BaseFragment(R.layout.fragment_games) {

    @Inject
    lateinit var gamesViewModel: GamesViewModel

    private val binding: FragmentGamesBinding by viewBinding()

    private lateinit var adapter: GamesAdapter

    override fun injectDependencies() {
        Injector.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideProgress()
        setupViews()
        setupObservers()
        loadData()
    }

    private fun setupViews() {
        adapter = GamesAdapter(requireContext(), mutableListOf(), object : GamesAdapter.GamesListener {
            override fun onGameClick(gameItemId: Int) {
                navigateToDetails(gameItemId)
            }
        })
        binding.rvGames.adapter = adapter

        binding.fabScrollToTop.hide()

        binding.fabScrollToTop.setOnClickListener {
            binding.rvGames.smoothAndFastScrollToPosition(0)
        }

        binding.rvGames.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if ((binding.rvGames.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() == 0) {
                    binding.fabScrollToTop.hide()
                } else {
                    binding.fabScrollToTop.show()
                }
            }
        })

        binding.swipeContainer.setOnRefreshListener {
            loadData()
        }
    }

    private fun setupObservers() {
        gamesViewModel.loading.observe(viewLifecycleOwner) {
            if (it) {
                showProgress()
            } else {
                hideProgress()
            }
        }
        gamesViewModel.errorMessage.observe(viewLifecycleOwner) {
            if (!it.isNullOrEmpty()) {
                showToast(it)
                binding.tvError.show()
            } else {
                binding.tvError.hide()
            }
        }
        gamesViewModel.games.observe(viewLifecycleOwner) {
            adapter.load(it)
        }
    }

    private fun loadData() {
        gamesViewModel.loadGames()
    }

    private fun navigateToDetails(gameItemId: Int) {
        val bundle = Bundle()
        bundle.putInt(Constants.GAME_DETAILS_BUNDLE, gameItemId)
        findNavController().navigate(R.id.action_navigate_to_game_details, bundle)
    }

    private fun showProgress() {
        if (!binding.swipeContainer.isRefreshing) {
            binding.progressBar.root.show()
        }
    }

    private fun hideProgress() {
        if (binding.swipeContainer.isRefreshing) {
            binding.swipeContainer.isRefreshing = false
        } else {
            binding.progressBar.root.hide()
        }
    }
}