package com.icell.testlibrary.mmoinfo.ui.gamedetails

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.viewbinding.library.fragment.viewBinding
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.icell.testlibrary.mmoinfo.Constants
import com.icell.testlibrary.mmoinfo.R
import com.icell.testlibrary.mmoinfo.databinding.FragmentGameDetailsBinding
import com.icell.testlibrary.mmoinfo.di.Injector
import com.icell.testlibrary.mmoinfo.extensions.hide
import com.icell.testlibrary.mmoinfo.extensions.show
import com.icell.testlibrary.mmoinfo.model.GameDetails
import com.icell.testlibrary.mmoinfo.ui.base.BaseFragment
import com.stfalcon.imageviewer.StfalconImageViewer
import javax.inject.Inject

class GameDetailsFragment : BaseFragment(R.layout.fragment_game_details) {

    @Inject
    lateinit var gameDetailsViewModel: GameDetailsViewModel

    private val binding: FragmentGameDetailsBinding by viewBinding()

    private lateinit var adapter: GameImagesAdapter

    private var gameId: Int? = null

    override fun injectDependencies() {
        Injector.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        gameId = arguments?.getInt(Constants.GAME_DETAILS_BUNDLE)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideProgress()
        setupViews()
        setupObservers()
        loadData()
    }

    private fun setupViews() {
        binding.ivBack.setOnClickListener {
            findNavController().popBackStack()
        }

        adapter = GameImagesAdapter(requireContext(), mutableListOf(), object : GameImagesAdapter.GameScreenShotListener {
            override fun onScreenShotClick(selectedImage: Int, allImages: List<String?>) {
                showImageViewer(selectedImage, allImages)
            }
        })

        binding.rvScreenShots.adapter = adapter
    }

    private fun setupObservers() {
        gameDetailsViewModel.loading.observe(viewLifecycleOwner) {
            if (it) {
                showProgress()
            } else {
                hideProgress()
            }
        }
        gameDetailsViewModel.errorMessage.observe(viewLifecycleOwner) {
            if (!it.isNullOrEmpty()) {
                showToast(it)
                showError()
            } else {
                hideError()
            }
        }
        gameDetailsViewModel.gameDetails.observe(viewLifecycleOwner) {
            fillViews(it)
        }
    }

    private fun loadData() {
        gameId?.let {
            gameDetailsViewModel.loadGameDetails(it)
        }
    }

    private fun fillViews(gameDetails: GameDetails) {
        Glide.with(requireContext()).load(gameDetails.thumbnail).centerCrop().placeholder(R.drawable.placeholder).into(binding.ivGame)

        binding.tvTitle.text = gameDetails.title
        binding.tvPublisher.text = gameDetails.publisher
        binding.tvDeveloper.text = gameDetails.developer
        binding.tvPlatform.text = gameDetails.platform
        binding.tvGenre.text = gameDetails.genre
        binding.tvRelease.text = gameDetails.releaseDate
        binding.tvDescription.text = Html.fromHtml(gameDetails.description, Html.FROM_HTML_MODE_COMPACT)

        gameDetails.screenshots?.let {
            adapter.load(it)
        }
    }

    private fun showImageViewer(selectedImage: Int, allImages: List<String?>) {
        StfalconImageViewer.Builder(context, allImages) { view: ImageView, image: String? ->
            Glide.with(requireContext()).load(image).placeholder(R.drawable.placeholder).into(view)
        }.withStartPosition(selectedImage)
            .withBackgroundColor(ContextCompat.getColor(requireContext(), R.color.white_80)).show()
    }

    private fun showError() {
        binding.tvError.show()
        binding.nsDetails.hide()
    }

    private fun hideError() {
        binding.tvError.hide()
        binding.nsDetails.show()
    }

    private fun showProgress() {
        binding.progressBar.root.show()
    }

    private fun hideProgress() {
        binding.progressBar.root.hide()
    }
}