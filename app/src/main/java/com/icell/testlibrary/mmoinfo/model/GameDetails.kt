package com.icell.testlibrary.mmoinfo.model

import com.google.gson.annotations.SerializedName

data class GameDetails(
    var id: Int? = null,
    var title: String? = null,
    var thumbnail: String? = null,
    var publisher: String? = null,
    @SerializedName("release_date") var releaseDate: String? = null,
    var description: String? = null,
    var genre: String? = null,
    var platform: String? = null,
    var developer: String? = null,
    var screenshots: List<GameScreenShot>? = null
)

data class GameScreenShot(
    var image: String? = null
)