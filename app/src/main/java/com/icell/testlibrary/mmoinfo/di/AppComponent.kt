package com.icell.testlibrary.mmoinfo.di

import com.icell.testlibrary.mmoinfo.service.WebService
import com.icell.testlibrary.mmoinfo.ui.gamedetails.GameDetailsFragment
import com.icell.testlibrary.mmoinfo.ui.games.GamesFragment
import com.icell.testlibrary.mmoinfo.ui.giveaways.GiveawaysFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(item: WebService)
    fun inject(item: GamesFragment)
    fun inject(item: GameDetailsFragment)
    fun inject(item: GiveawaysFragment)
}