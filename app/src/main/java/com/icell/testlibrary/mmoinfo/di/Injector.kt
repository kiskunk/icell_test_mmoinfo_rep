package com.icell.testlibrary.mmoinfo.di

import com.icell.testlibrary.mmoinfo.MMOInfoApp
import com.icell.testlibrary.mmoinfo.ui.gamedetails.GameDetailsFragment
import com.icell.testlibrary.mmoinfo.ui.games.GamesFragment
import com.icell.testlibrary.mmoinfo.ui.giveaways.GiveawaysFragment

object Injector {

    private lateinit var appComponent: AppComponent

    fun init(application: MMOInfoApp) {
        appComponent = DaggerAppComponent.builder().appModule(AppModule(application)).build()
    }

    fun inject(forItem: GamesFragment) {
        appComponent.inject(forItem)
    }

    fun inject(forItem: GameDetailsFragment) {
        appComponent.inject(forItem)
    }

    fun inject(forItem: GiveawaysFragment) {
        appComponent.inject(forItem)
    }
}