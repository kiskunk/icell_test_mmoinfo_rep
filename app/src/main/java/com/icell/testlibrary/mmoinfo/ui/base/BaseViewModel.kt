package com.icell.testlibrary.mmoinfo.ui.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.icell.testlibrary.mmoinfo.interfaces.DisposableStore
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel : ViewModel(), DisposableStore {

    private val compositeDisposable = CompositeDisposable()
    val loading = MutableLiveData(false)
    val errorMessage = MutableLiveData("")

    override fun store(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun dispose() {
        compositeDisposable.dispose()
    }

    override fun onCleared() {
        super.onCleared()
        dispose()
    }
}