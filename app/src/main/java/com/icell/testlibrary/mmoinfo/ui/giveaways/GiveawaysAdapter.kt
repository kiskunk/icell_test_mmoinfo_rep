package com.icell.testlibrary.mmoinfo.ui.giveaways

import android.content.Context
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.icell.testlibrary.mmoinfo.R
import com.icell.testlibrary.mmoinfo.model.GiveawayItem

class GiveawaysAdapter(
    private var context: Context,
    private var items: MutableList<GiveawayItem> = mutableListOf(),
    private val listener: GiveawayUrlListener?
) : RecyclerView.Adapter<GiveawaysAdapter.ViewHolder>() {

    interface GiveawayUrlListener {
        fun onGiveawayUrlClick(giveawayUrl: String)
    }

    fun load(giveaways: List<GiveawayItem>) {
        items.clear()

        if (giveaways.isEmpty()) {
            notifyDataSetChanged()
            return
        }

        items.addAll(giveaways)

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.giveaway_item_layout, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val giveaway = items[position]

        Glide.with(context).load(giveaway.thumbnail).centerCrop().placeholder(R.drawable.placeholder).into(viewHolder.ivGiveaway)

        viewHolder.tvTitle.text = giveaway.title
        viewHolder.tvKeys.text = context.getString(R.string.giveaway_keys_left, giveaway.keysLeft)
        viewHolder.tvDescription.text = Html.fromHtml(giveaway.description, Html.FROM_HTML_MODE_COMPACT)

        viewHolder.root.setOnClickListener {
            giveaway.giveawayUrl?.let {
                listener?.onGiveawayUrlClick(it)
            }
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val root: LinearLayout = view.findViewById(R.id.llRoot)
        val ivGiveaway: ImageView = view.findViewById(R.id.ivGiveaway)
        val tvTitle: TextView = view.findViewById(R.id.tvTitle)
        val tvKeys: TextView = view.findViewById(R.id.tvKeys)
        val tvDescription: TextView = view.findViewById(R.id.tvDescription)
    }
}