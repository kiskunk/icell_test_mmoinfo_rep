package com.icell.testlibrary.mmoinfo.ui.giveaways

import androidx.lifecycle.MutableLiveData
import com.icell.testlibrary.mmoinfo.extensions.store
import com.icell.testlibrary.mmoinfo.model.GiveawayItem
import com.icell.testlibrary.mmoinfo.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GiveawaysViewModel @Inject constructor(private val giveawaysRepository: GiveawaysRepository) : BaseViewModel() {

    val giveaways = MutableLiveData<List<GiveawayItem>>(mutableListOf())

    fun loadGiveaways() {
        loading.postValue(true)
        errorMessage.postValue("")
        giveawaysRepository.loadGiveaways()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loading.postValue(false)
                giveaways.postValue(it)
            }, { t: Throwable? ->
                loading.postValue(false)
                errorMessage.postValue(t?.message)
            }).store(this)
    }
}