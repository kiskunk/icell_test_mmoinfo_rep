package com.icell.testlibrary.mmoinfo.ui.gamedetails

import androidx.lifecycle.MutableLiveData
import com.icell.testlibrary.mmoinfo.extensions.store
import com.icell.testlibrary.mmoinfo.model.GameDetails
import com.icell.testlibrary.mmoinfo.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GameDetailsViewModel @Inject constructor(private val gameDetailsRepository: GameDetailsRepository) : BaseViewModel() {

    val gameDetails = MutableLiveData<GameDetails>()

    fun loadGameDetails(gameId: Int) {
        loading.postValue(true)
        errorMessage.postValue("")
        gameDetailsRepository.loadGameDetails(gameId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loading.postValue(false)
                gameDetails.postValue(it)
            }, { t: Throwable? ->
                loading.postValue(false)
                errorMessage.postValue(t?.message)
            }).store(this)
    }
}