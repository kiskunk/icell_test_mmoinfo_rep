package com.icell.testlibrary.mmoinfo.interfaces

import io.reactivex.disposables.Disposable

interface DisposableStore {
    fun store(disposable: Disposable)
    fun dispose()
}