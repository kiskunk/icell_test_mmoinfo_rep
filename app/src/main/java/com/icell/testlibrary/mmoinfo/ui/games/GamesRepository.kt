package com.icell.testlibrary.mmoinfo.ui.games

import com.icell.testlibrary.mmoinfo.service.WebService
import javax.inject.Inject

class GamesRepository @Inject constructor(private val webService: WebService) {

    fun loadGames() = webService.getWebApi().retrieveGames()
}