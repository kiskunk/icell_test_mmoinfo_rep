package com.icell.testlibrary.mmoinfo.ui.games

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.icell.testlibrary.mmoinfo.R
import com.icell.testlibrary.mmoinfo.model.GameItem

class GamesAdapter(
    private var context: Context,
    private var items: MutableList<GameItem> = mutableListOf(),
    private val listener: GamesListener?
) : RecyclerView.Adapter<GamesAdapter.ViewHolder>() {

    interface GamesListener {
        fun onGameClick(gameItemId: Int)
    }

    fun load(games: List<GameItem>) {
        items.clear()

        if (games.isEmpty()) {
            notifyDataSetChanged()
            return
        }

        items.addAll(games)

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.game_item_layout, parent, false))
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val gameItem = items[position]

        Glide.with(context).load(gameItem.thumbnail).centerCrop().placeholder(R.drawable.placeholder).into(viewHolder.ivGame)

        viewHolder.tvTitle.text = gameItem.title
        viewHolder.tvInfo.text = gameItem.getInfo()

        viewHolder.root.setOnClickListener {
            gameItem.id?.let {
                listener?.onGameClick(it)
            }
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val root: CardView = view.findViewById(R.id.cvRoot)
        val ivGame: ImageView = view.findViewById(R.id.ivGame)
        val tvTitle: TextView = view.findViewById(R.id.tvTitle)
        val tvInfo: TextView = view.findViewById(R.id.tvInfo)
    }
}