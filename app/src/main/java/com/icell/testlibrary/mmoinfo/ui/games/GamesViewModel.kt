package com.icell.testlibrary.mmoinfo.ui.games

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.icell.testlibrary.mmoinfo.extensions.store
import com.icell.testlibrary.mmoinfo.model.GameItem
import com.icell.testlibrary.mmoinfo.ui.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GamesViewModel @Inject constructor(private val gamesRepository: GamesRepository) : BaseViewModel() {

    val games = MutableLiveData<List<GameItem>>(mutableListOf())

    fun loadGames() {
        loading.postValue(true)
        errorMessage.postValue("")
        gamesRepository.loadGames()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                loading.postValue(false)
                games.postValue(it)
            }, { t: Throwable? ->
                loading.postValue(false)
                errorMessage.postValue(t?.message)
            }).store(this)
    }
}