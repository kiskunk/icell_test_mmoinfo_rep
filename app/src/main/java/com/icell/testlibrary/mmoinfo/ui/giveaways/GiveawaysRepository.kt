package com.icell.testlibrary.mmoinfo.ui.giveaways

import com.icell.testlibrary.mmoinfo.service.WebService
import javax.inject.Inject

class GiveawaysRepository @Inject constructor(private val webService: WebService) {

    fun loadGiveaways() = webService.getWebApi().retrieveGiveaways()
}