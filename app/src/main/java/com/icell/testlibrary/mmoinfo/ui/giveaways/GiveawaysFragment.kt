package com.icell.testlibrary.mmoinfo.ui.giveaways

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.viewbinding.library.fragment.viewBinding
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.icell.testlibrary.mmoinfo.R
import com.icell.testlibrary.mmoinfo.databinding.FragmentGiveawaysBinding
import com.icell.testlibrary.mmoinfo.di.Injector
import com.icell.testlibrary.mmoinfo.extensions.hide
import com.icell.testlibrary.mmoinfo.extensions.show
import com.icell.testlibrary.mmoinfo.extensions.smoothAndFastScrollToPosition
import com.icell.testlibrary.mmoinfo.ui.base.BaseFragment
import timber.log.Timber
import javax.inject.Inject


class GiveawaysFragment : BaseFragment(R.layout.fragment_giveaways) {

    @Inject
    lateinit var giveawaysViewModel: GiveawaysViewModel

    private val binding: FragmentGiveawaysBinding by viewBinding()

    private lateinit var adapter: GiveawaysAdapter

    override fun injectDependencies() {
        Injector.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hideProgress()
        setupViews()
        setupObservers()
        loadData()
    }

    private fun setupViews() {
        adapter = GiveawaysAdapter(requireContext(), mutableListOf(), object : GiveawaysAdapter.GiveawayUrlListener {
            override fun onGiveawayUrlClick(giveawayUrl: String) {
                openLink(giveawayUrl)
            }
        })
        binding.rvGiveaways.adapter = adapter

        binding.fabScrollToTop.hide()

        binding.fabScrollToTop.setOnClickListener {
            binding.rvGiveaways.smoothAndFastScrollToPosition(0)
        }

        binding.rvGiveaways.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if ((binding.rvGiveaways.layoutManager as LinearLayoutManager).findFirstCompletelyVisibleItemPosition() == 0) {
                    binding.fabScrollToTop.hide()
                } else {
                    binding.fabScrollToTop.show()
                }
            }
        })

        binding.swipeContainer.setOnRefreshListener {
            loadData()
        }
    }

    private fun setupObservers() {
        giveawaysViewModel.loading.observe(viewLifecycleOwner) {
            if (it) {
                showProgress()
            } else {
                hideProgress()
            }
        }
        giveawaysViewModel.errorMessage.observe(viewLifecycleOwner) {
            if (!it.isNullOrEmpty()) {
                showToast(it)
                binding.tvError.show()
            } else {
                binding.tvError.hide()
            }
        }
        giveawaysViewModel.giveaways.observe(viewLifecycleOwner) {
            adapter.load(it)
        }
    }

    private fun loadData() {
        giveawaysViewModel.loadGiveaways()
    }

    private fun openLink(giveawayUrl: String) {
        try {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(giveawayUrl)))
        } catch (e: Exception) {
            showToast(getString(R.string.could_not_open_giveaway))
            Timber.e(e)
        }
    }

    private fun showProgress() {
        if (!binding.swipeContainer.isRefreshing) {
            binding.progressBar.root.show()
        }
    }

    private fun hideProgress() {
        if (binding.swipeContainer.isRefreshing) {
            binding.swipeContainer.isRefreshing = false
        } else {
            binding.progressBar.root.hide()
        }
    }
}