package com.icell.testlibrary.mmoinfo.model

import com.google.gson.annotations.SerializedName

data class GameItem(
    var id: Int? = null,
    var title: String? = null,
    var thumbnail: String? = null,
    var publisher: String? = null,
    @SerializedName("release_date") var releaseDate: String? = null
) {
    fun getInfo(): String {
        return ((publisher ?: "") + " " + (releaseDate ?: "")).trim()
    }
}