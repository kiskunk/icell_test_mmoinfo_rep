package com.icell.testlibrary.mmoinfo.service

import com.icell.testlibrary.mmoinfo.BuildConfig
import com.icell.testlibrary.mmoinfo.Constants
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class WebService @Inject constructor() {

    private var httpClient: OkHttpClient
    private var retrofit: Retrofit
    private var webApi: WebApi

    companion object {

        private var instance: WebService? = null

        fun getInstance(): WebService {
            if (instance == null) {
                instance = WebService()
            }
            return instance!!
        }
    }

    init {
        httpClient = OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor())
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()

        webApi = retrofit.create(WebApi::class.java)
    }

    private fun loggingInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return interceptor
    }

    fun getWebApi(): WebApi {
        return webApi
    }
}