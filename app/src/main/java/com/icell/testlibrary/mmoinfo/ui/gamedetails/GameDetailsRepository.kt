package com.icell.testlibrary.mmoinfo.ui.gamedetails

import com.icell.testlibrary.mmoinfo.service.WebService
import javax.inject.Inject

class GameDetailsRepository @Inject constructor(private val webService: WebService) {

    fun loadGameDetails(gameId: Int) = webService.getWebApi().loadGameDetails(gameId)
}