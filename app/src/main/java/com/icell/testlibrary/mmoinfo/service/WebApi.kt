package com.icell.testlibrary.mmoinfo.service

import com.icell.testlibrary.mmoinfo.model.GameDetails
import com.icell.testlibrary.mmoinfo.model.GameItem
import com.icell.testlibrary.mmoinfo.model.GiveawayItem
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface WebApi {

    @GET("api1/games?sort-by=alphabetical")
    fun retrieveGames(): Single<List<GameItem>>

    @GET("api1/giveaways")
    fun retrieveGiveaways(): Single<List<GiveawayItem>>

    @GET("api1/game")
    fun loadGameDetails(@Query("id") gameId: Int): Single<GameDetails>
}