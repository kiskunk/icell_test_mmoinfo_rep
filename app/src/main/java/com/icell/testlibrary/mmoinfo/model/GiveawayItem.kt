package com.icell.testlibrary.mmoinfo.model

import com.google.gson.annotations.SerializedName

data class GiveawayItem(
    var id: Int? = null,
    var title: String? = null,
    @SerializedName("keys_left") var keysLeft: String? = null,
    var thumbnail: String? = null,
    @SerializedName("short_description") var description: String? = null,
    @SerializedName("giveaway_url") var giveawayUrl: String? = null
)