package com.icell.testlibrary.mmoinfo.di

import android.content.Context
import com.icell.testlibrary.mmoinfo.MMOInfoApp
import com.icell.testlibrary.mmoinfo.service.WebApi
import com.icell.testlibrary.mmoinfo.service.WebService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val appContext: MMOInfoApp) {

    @Provides
    fun providesContext(): Context {
        return appContext
    }

    @Provides
    @Singleton
    fun providesWebService(): WebService {
        return WebService.getInstance()
    }

    @Provides
    @Singleton
    fun providesWebApi(): WebApi {
        return WebService.getInstance().getWebApi()
    }
}